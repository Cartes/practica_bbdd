INSERT INTO MAPA VALUES
('Grieta del Invocador',10,'Nexo'),
('Bosque Retorcido',6,'Nexo'),
('Abismo de los Lamentos',10,'Nexo'),
('Cicatriz de Cristal',10,'Puntos'),
('Bombardeo al nexo',10,'Nexo');

INSERT INTO OBJETO VALUES
('Grebas de Berserker','No',1100,'Botas'),
('Hidra Titánica','Si',3500,'Daño'),
('Malla de Espinas','No',2900,'Armadura'),
('Armadura de Warmog','No',2850,'Regeneración de Vida'),
('Reloj de Arena de Zhonya','Si',2900,'Poder de Habilidad');

INSERT INTO DINERO VALUES
(4285,4685,1250),
(1964,4056,4000),
(1348,8642,2900),
(9431,4536,2850),
(1358,2987,2900);

INSERT INTO RANGO VALUES
(1272,'Plata','III'),
(2242,'Diamante','I'),
(2834,'Master','II'),
(1623,'Oro','IV'),
(850,'Bronce','V');

INSERT INTO RUNA VALUES

('Ataque Intensificado','Precision'),
('Aery','Brujería'),
('Cleptomanía','Inspiración'),
('Depredador','Dominación'),
('Demoler','Valor');

INSERT INTO PRINCIPAL VALUES
('Ataque Intensificado','Si'),
('Compás Letal','No'),
('Pies Veloces','No'),
('Conquistador','Si'),
('Super Curación','No');

INSERT INTO SECUNDARIA VALUES
('Cometa Arcano','Si'),
('Irrupción de Fase','Si'),
('Orbe Anulador','Si'),
('Banda de Mana','Si'),
('Conquistador','No');

INSERT INTO PERSONAJE VALUES
('Kindred','Jungla',2,'Asesino','Grieta del Invocador',23,12),
('Irelia','Mid',6,'Mago','Bosque Retorcido',12,60),
('Kled','Top',1,'Tanque','Abismo de los lamentos',45,23),
('Lucian','Adc',3,'Luchador','Cicatriz de Cristal',23,45),
('Rakan','Supp',3,'Tirador','Bombardeo al Nexo',12,14);

INSERT INTO USUARIO(nombreUsuario, nombre, apellido, icono, nivel, correo, mmr_RANGO) VALUES
('faker','Pepe','Rodriguez','Fnatic',35,'peperodriguez@hotmail.com',1272),
('windowe_345','Benito','Camelas','SKT',59,'benitocamelas@hotmail.com',2242),
('elmillor','Sergio','Gutierrez','All star 2017',62,'sergiogutierrez@hotmail.com',2834),
('micky_xD','Sara','Fernandez','Rey Poro',67,'sarafernandez@gmail.com',1623),
('Hide un Bush','Elena','Lopez','Bronce',120,'elenalopez@gmail.com',850);

INSERT INTO INVITA VALUES
('faker','nexxorkx'),
('faker','windowe_345'),
('faker','elmillor'),
('faker','micky_xD'),
('Hide un bush','faker');

INSERT INTO LLEVAN VALUES
('Irelia','Aery'),
('Kindred','Ataque Intensificado'),
('Kled','Cleptomanía'),
('Lucian','Demoler'),
('Rakan','Depredador');

INSERT INTO TIENE VALUES
(4285,4685,1250,'peperodriguez@hotmail.com'),
(1964,4056,4000,'benitohernandez@hotmail.com'),
(1348,8642,2900,'sergiogutierrez@hotmail.com'),
(9431,4536,2850,'sarafernandez@hotmail.com'),
(1358,2987,2900,'Elenalopez@hotmail.com');

INSERT INTO USAN VALUES
('Kindred','Armadura de Warmog'),
('Irelia','Hidra Titánica'),
('Kled','Malla de Espinas'),
('Lucian','Armadura de Warmog'),
('Rakan','Reloj de Arena de Zhonya');




















