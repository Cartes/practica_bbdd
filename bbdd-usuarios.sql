use bbddantigua;
DROP VIEW IF EXISTS Mi_cuenta_Faker;
CREATE VIEW Mi_cuenta_Faker AS SELECT * FROM USUARIO WHERE ID='1';

/*USUARIOS*/
DROP USER IF EXISTS 'administrador'@'localhost';
CREATE USER 'administrador'@'localhost' IDENTIFIED BY 'Admin12';
GRANT ALL ON *.* TO 'administrador'@'localhost';

DROP USER IF EXISTS 'jugador'@'localhost'; /*Usario*/
CREATE USER 'jugador'@'localhost' IDENTIFIED BY 'Elcejas1234';
GRANT SELECT ON bbddantigua.* TO 'jugador'@'localhost';
GRANT UPDATE(NombreUsuario, Icono, Correo, Nombre, Apellido) ON bbddantigua.USUARIO TO 'jugador'@'localhost';

/*Ejemplo de jugador */
DROP USER IF EXISTS 'faker'@'localhost';
CREATE USER 'faker'@'localhost' IDENTIFIED BY '1234';
GRANT SELECT ON bbddantigua.Mi_cuenta_Faker TO 'faker'@'localhost';
GRANT SELECT ON bbddantigua.DINERO TO 'faker'@'localhost';
GRANT SELECT ON bbddantigua.INVITA TO 'faker'@'localhost';
GRANT SELECT ON bbddantigua.LLEVAN TO 'faker'@'localhost';
GRANT SELECT ON bbddantigua.MAPA TO 'faker'@'localhost';
GRANT SELECT ON bbddantigua.OBJETO TO 'faker'@'localhost';
GRANT SELECT ON bbddantigua.PERSONAJE TO 'faker'@'localhost';
GRANT SELECT ON bbddantigua.PRINCIPAL TO 'faker'@'localhost';
GRANT SELECT ON bbddantigua.RANGO TO 'faker'@'localhost';
GRANT SELECT ON bbddantigua.RUNA TO 'faker'@'localhost';
GRANT SELECT ON bbddantigua.SECUNDARIA TO 'faker'@'localhost';
GRANT SELECT ON bbddantigua.TIENE TO 'faker'@'localhost';
GRANT SELECT ON bbddantigua.USAN TO 'faker'@'localhost';
GRANT UPDATE(NombreUsuario, Icono, Correo, Nombre, Apellido) ON bbddantigua.Mi_cuenta_Faker TO 'faker'@'localhost';

DROP USER IF EXISTS 'rioters'@'localhost';
CREATE USER 'rioters'@'localhost' IDENTIFIED BY 'tebaneo1234';
GRANT SELECT ON bbddantigua.* TO 'rioters'@'localhost';        /*UPDATE Y DELETE POR QUE ELIMINAN USUARIOS Y LES PUEDEN CAMBIAR DATOS EN CASO DE BANEO,ETC.*/
GRANT UPDATE(NombreUsuario, Icono, Correo, Nombre, Apellido), DELETE ON bbddantigua.USUARIO TO 'rioters'@'localhost';

/* USUARIO DESARROLADOR */
DROP USER IF EXISTS 'desarrollador'@'localhost';
CREATE USER 'desarrollador'@'localhost' IDENTIFIED BY 'Des@rrollador1234';
GRANT SELECT ON bbddantigua.* TO 'desarrollador'@'localhost';
GRANT CREATE, UPDATE, DELETE, DROP ON bbddantigua.* TO 'desarrollador'@'localhost'; /*LOS PERMISOS QUE LE DAMOS A ESTOS USUARIOS SON PARA PROBAR Y METER NUEVAS COSAS */

/*Usuario anonimo donde puede mirar datos sobre otros*/
DROP USER IF EXISTS 'anonimo'@'localhost';
CREATE USER 'anonimo'@'localhost';
GRANT SELECT ON bbddantigua.LLEVAN TO 'anonimo'@'localhost';
GRANT SELECT ON bbddantigua.PERSONAJE TO 'anonimo'@'localhost';
GRANT SELECT ON bbddantigua.LLEVAN TO 'anonimo'@'localhost';
GRANT SELECT ON bbddantigua.OBJETO TO 'anonimo'@'localhost';
GRANT SELECT ON bbddantigua.MAPA TO 'anonimo'@'localhost';
GRANT SELECT ON bbddantigua.RANGO TO 'anonimo'@'localhost';
GRANT SELECT ON bbddantigua.RUNA TO 'anonimo'@'localhost';
GRANT SELECT ON bbddantigua.USAN TO 'anonimo'@'localhost';

