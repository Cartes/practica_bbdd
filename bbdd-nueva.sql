USE bbddantigua;

/*Añadimos Minutos y segundos a la tabala PERSONAJE y posteriormente eliminamos la tabal tiempo*/
ALTER TABLE PERSONAJE ADD Minutos TINYINT;
ALTER TABLE PERSONAJE ADD Segundos TINYINT;

/*Corregimos los errores de la tabla usuario y añadimos la unique key y creamos una ID y la declaramos primary key con auto increment */
ALTER TABLE USUARIO DROP PRIMARY KEY;
ALTER TABLE USUARIO ADD Apellido VARCHAR(20);
ALTER TABLE USUARIO ADD ID INT AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE USUARIO ADD NombreUsuario VARCHAR(20) NOT NULL UNIQUE;
ALTER TABLE USUARIO DROP FOREIGN KEY FK_USUARIO_CUENTA;
ALTER TABLE USUARIO DROP Correo_CUENTA;                           /* Eliminamos la tabala CUENTA por tanto también le quitamos todas la foreign keys del resto de tablas */
ALTER TABLE USUARIO ADD Correo VARCHAR(80);
ALTER TABLE USUARIO ADD CONSTRAINT UC_USUARIO UNIQUE(Correo);     /* Añadimos UNIQUE para evitar que se repitan los emails */

ALTER TABLE TIENE DROP FOREIGN KEY FK_TIENE_CUENTA;
ALTER TABLE TIENE DROP Correo_CUENTA;
ALTER TABLE TIENE ADD Correo VARCHAR(80);

ALTER TABLE PERSONAJE DROP FOREIGN KEY FK_PERSONAJE_RANGO;
ALTER TABLE PERSONAJE DROP mmr_RANGO;

/*Eliminamos las tablas erroneamente optimizadas*/
DROP TABLE CUENTA;

